import React from 'react';
import PropTypes from 'prop-types';
import Modal from '@mui/material/Modal';
import CustomButton from '../CustomButton';
import { SuccessErrorBox } from './SuccessErrorModal.style';
import successImg from '../../assets/images/success.png';
import errorImg from '../../assets/images/error.png';

const SuccessErrorModal = ({ isSuccess, showModal, setShowModal, response }) => {
    const handleClose = () => setShowModal(false);

    return (
        <Modal
            open={showModal}
            onClose={handleClose}
        >
            <SuccessErrorBox>
                <img src={ isSuccess ? successImg : errorImg } alt={ isSuccess ? 'success image' : 'error image' } width="80" height="80" />
                <h2>{ isSuccess ? 'Success' : 'Error' }</h2>
                <h4>{ response }</h4>
                <CustomButton variant={ isSuccess ? 'contained' : 'outlined' } onClick={handleClose} >
                    Close
                </CustomButton>
            </SuccessErrorBox>
        </Modal>
    );
};

SuccessErrorModal.propTypes = {
    isSuccess: PropTypes.bool.isRequired,
    setShowModal: PropTypes.func.isRequired,
    showModal: PropTypes.bool.isRequired,
    response: PropTypes.string.isRequired,
};
  

export default SuccessErrorModal;