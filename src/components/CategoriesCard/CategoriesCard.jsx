import React from 'react';
import { Link } from 'react-router-dom';
import Card from '@mui/material/Card';
import { CardContent, Logo } from './CategoriesCard.style';

const CategoriesCard = ({ src, title }) => {
  const styledCard = {
    width: '230px',
    margin: '15px 10px',
    cursor: 'pointer',
    background: '#fff',
    ':hover': {
      boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px',
    },
    '@media (max-width: 1000px)': {
      width: '400px',
    },
    '@media (max-width: 768px)': {
      width: '100%',
    },
  };
  return (
    <Card sx={styledCard}>
      <Link to="/post-task">
        <CardContent>
          <Logo src={src} />
          <h3>{title}</h3>
        </CardContent>
      </Link>
    </Card>
  );
};

export default CategoriesCard;
