import styled from 'styled-components';
import CustomButton from '../CustomButton';

export const Buttonstyle = styled(CustomButton)`
  background-color: white;
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 2em;
  border: 5px solid #52ab98;
  color: #52ab98;
  border-radius: 5px;
`;
