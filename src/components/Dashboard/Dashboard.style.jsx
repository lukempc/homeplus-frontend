import Box from '@mui/material/Box';
import { styled } from '@mui/system';

export const CustomBox = styled(Box)`
  width: 852px;
  overflow: scroll;
  margin-top: 1rem;
  overflow-x: hidden;
  @media (max-width: 768px) {
    width: 100vw;
    overflow: scroll;
  }
`;

export const DashboardBox = styled(Box)`
  button {
    margin: 0;
  }
  display: flex;
  flex-direction: row;
  width: 1152px;
  justify-content: center;
  color: #444;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  min-height: 94vh;
  padding-top: 2px;
  overflow: hidden;
  @media (max-width: 768px) {
    width: 100vw;
    height: 100vh;
  }
`;
