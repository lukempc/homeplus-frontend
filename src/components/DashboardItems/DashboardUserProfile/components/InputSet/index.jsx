import React from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';

const InputSet = ({ title, name, formData, setFormData }) => {
  const handleChange = (e) => {
    e.preventDefault();
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', margin: '10px' }}>
      <TextField
        required
        name={name}
        label={title}
        onChange={handleChange}
        value={formData[name] ? formData[name] : ''}
        id="outlined-basic"
      />
    </Box>
  );
};

InputSet.propTypes = {
  title: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  formData: PropTypes.object.isRequired,
  setFormData: PropTypes.func.isRequired,
};

export default InputSet;
