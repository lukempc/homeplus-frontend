import { React } from 'react';
import PropTypes from 'prop-types';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';

const SelectionSet = ({ formData, setFormData }) => {
  const selectItems = ['Male', 'Female', 'Non-binary', 'I prefer not to say'];

  const handleChange = (e) => {
    e.preventDefault();
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  return (
    <FormControl
      sx={{ display: 'flex', flexDirection: 'column', margin: '10px', maxWidth: '600px' }}
      className="selectionRow"
    >
      <InputLabel id="gender">Gender</InputLabel>
      <Select
        labelId="gender"
        name="gender"
        value={formData.gender ? formData.gender : ''}
        label="Gender"
        required
        sx={{ width: '100%' }}
        onChange={handleChange}
      >
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
        {selectItems.map((item, index) => (
          <MenuItem value={item} key={index}>
            {item}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

SelectionSet.propTypes = {
  formData: PropTypes.object.isRequired,
  setFormData: PropTypes.func.isRequired,
};

export default SelectionSet;
