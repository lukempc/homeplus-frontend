import * as React from 'react';
import PropTypes from 'prop-types';
import { Card } from './DashboardCard.style';

export default function DashboardCard({ title, theme, count }) {
  return (
    <Card>
      <div className={`dashboardCard ${theme}`}>
        <p className="count">{count}</p>
        <p className="title">{title}</p>
      </div>
    </Card>
  );
}

DashboardCard.propTypes = {
  title: PropTypes.string.isRequired,
  theme: PropTypes.string,
  count: PropTypes.number,
};
