import styled from 'styled-components';

const primary = '#52ab98';
const darkGreen = '#2b6777';

export const Card = styled.div`
  width: 200px;
  height: 120px;
  margin-top: 20px;
  margin-left: 40px;
  margin-bottom: 40px;
  .dashboardCard {
    width: 100%;
    height: 100%;
    border-radius: 20px;
    p {
      color: white;
      margin: 0px;
      padding-top: 10px;
      padding-left: 20px;
      display: flex;
      flex-direction: column;
      justify-content: center;
    }
  }
  .primary {
    background-color: ${primary};
  }
  .secondary {
    background-color: ${darkGreen};
  }
  .count {
    font-size: 55px;
  }
  .title {
    font-size: 20px;
  }
`;
