import React from 'react';
import PropTypes from 'prop-types';
import { Box } from './TaskerDisableInfo.style';
import pic from '../../../../assets/EmptyBox.jpg';

export default function TaskerDiasbleInfo({ message }) {
  return (
    <Box>
        <img src={pic} alt="Logo" />
        <p> { message } </p>
    </Box>
  );
}

TaskerDiasbleInfo.propTypes = {
  message: PropTypes.string.isRequired
};