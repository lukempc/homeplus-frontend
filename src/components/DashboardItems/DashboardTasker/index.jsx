import React from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import TaskCalendar from '../TaskCalendar';

const DashboardTasker = ({ tasksOfUser, tasksOfTasker, isReloadData, setIsReloadData }) => (
  <Box sx={{ width: '90%' }}>
    <TaskCalendar
      tasksOfUser={tasksOfUser}
      tasksOfTasker={tasksOfTasker}
      isReloadData={isReloadData}
      setIsReloadData={setIsReloadData}
    />
  </Box>
);

DashboardTasker.propTypes = {
  tasksOfUser: PropTypes.any,
  tasksOfTasker: PropTypes.any,
  isReloadData: PropTypes.bool,
  setIsReloadData: PropTypes.func,
};

export default DashboardTasker;
