import styled from 'styled-components';

export const Box = styled.div`
  display: flex; 
  justify-content: center;
  background-color:#333535;
  p{
    color:#BBC2DC;
    font-size: 13px;
  }
  
`;