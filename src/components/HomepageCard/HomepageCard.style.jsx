import styled from 'styled-components';
export const CardContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 20px;
  padding: 4%;
`;

export const Content = styled.div`
  margin: 0px;
  line-height: 1.6;
  text-align: center;
`;

export const ButtonWrapper = styled.div`
  margin-bottom: 15px;
`;

export const Logo = styled.img`
  margin: 5px;
  max-width: 80px;
  height: auto;
`;
