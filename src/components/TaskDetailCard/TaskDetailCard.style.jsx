import styled from 'styled-components';

export const DetailCard = styled.div`
  h1,
  h3,
  span {
    font-weight: 500;
  }
  overflow-y: scroll;
  width: 100%;
  height: 94vh;
  position: relative;
  background-color: #fff;
  left: 50%;
  transform: translateX(-50%);
  padding-right: 20px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  .longLine {
    width: 90%;
    border-top: 1.5px solid #f2f2f2;
    position: relative;
    left: 50%;
    transform: translateX(-50%);
  }
  > div {
    display: flex;
    align-items: center;
    justify-content: space-around;
    width: 100%;
    margin-top: 2%;
  }
  .taskStatus {
    padding: 3%;
    width: 50%;
    justify-content: start;
    gap: 10px;
    margin-top: 2rem;
    padding-left: 2rem;
    .mobileCancel {
      display: none;
    }
  }
  .cardHeader {
    margin-top: -0.5rem;
    margin-bottom: 0.8rem;
    padding-left: 3%;
    width: 95%;
    .cardTitle {
      color: #444;
      display: flex;
      align-items: center;
      justify-content: space-between;
      width: 60%;
      padding: 3%;
      > div {
        display: flex;
      }
    }
  }
  .cardImportInfo {
    display: inline-grid;
    grid-template-columns: auto auto auto;
    height: 75px;
  }
  .cardOtherInfo {
    justify-content: space-between;
    align-items: start;
    display: inline;
    > div {
      display: flex;
    }
    .taskInfo {
      display: flex;
      width: 92%;
      min-height: 28rem;
      margin-left: 30px;
      .taskSubInfo {
        margin-top: 3%;
        width: 15%;
        display: inline;
        > div {
          padding: 15%;
        }
      }
      .description {
        line-height: 1.6;
        width: 70%;
        padding: 3%;
      }
    }
    .offersBox {
      display: inline-block;
      width: 90%;
      padding: 3%;
      margin-left: 30px;
    }
    form {
      margin-bottom: 25px;
    }
    .commentArea {
      display: inline-block;
      width: 90%;
      position: relative;
      justify-content: start;
      margin-left: 30px;
      margin-bottom: 30px;
      padding: 3%;
      > div {
        margin-left: 15px;
      }
      .commentBox {
        width: 95%;
        position: relative;
        justify-content: space-around;
        button {
          margin-left: 15px;
        }
      }
    }
  }
  @media (max-width: 768px) {
    height: 100vh;
    h1 {
      font-size: 23px;
    }
    h3 {
      font-size: 18px;
    }
    span {
      font-size: 14px;
    }
    .cardHeader {
      width: 100%;
      padding: 3%;
      button {
        display: none;
      }
    }
    .cardImportInfo {
      width: 85%;
      height: 265px;
      display: block;
      padding-left: 15px;
      padding-bottom: 15px;
      > div {
        margin-top: 3px;
        padding: 3%;
      }
    }
    .cardOtherInfo {
      .taskInfo {
        height: none;
        margin-left: 15px;
        display: block;
        .description {
          width: 90%;
        }
        .taskSubInfo {
          h3 {
            padding: 3%;
          }
          > div {
            padding: 3%;
          }
        }
      }
      .offersBox {
        margin-left: 15px;
        width: 86%;
      }
      .commentArea {
        margin-left: 15px;
      }
    }
    .taskStatus {
      .mobileCancel {
        display: flex;
        position: absolute;
        right: 8%;
      }
    }
  }
`;

export const Container = styled.div`
  width: 100%;
  height: 82vh;
  position: absolute;
  justify-content: center;
  align-items: center;
  align-above: 1px;
  z-index: 9999;
`;
