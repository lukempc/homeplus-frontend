import styled from 'styled-components';

export const TaskInfoWindow = styled.div`
  width: 300px;
  padding: 5%;
  color: #444;
  h3,
  p {
    font-weight: 500;
  }
  > div {
    display: flex;
    align-items: center;
    justify-content: space-between;
    .user {
      margin-top: 15px;
      display: flex;
      align-items: center;
      span {
        margin-left: 15px;
      }
    }
    .budgetBox {
      height: 80px;
      background-color: #f2f2f2;
      border-radius: 3px;
      padding-left: 5%;
      padding-right: 5%;
      margin-right: 10px;
      p {
        text-align: center;
        font-size: 0.9rem;
      }
    }
  }
  @media (max-width: 768px) {
    width: 260px;
  }
`;

export const clusterStyle = [
  {
    textColor: 'white',
    textSize: '20px',
    height: 53,
    url: 'https://marker.nanoka.fr/map_cluster-0d9ddb-50.svg',
    width: 53,
  },
  {
    textColor: 'white',
    height: 56,
    url: 'https://marker.nanoka.fr/map_cluster-52AB98-60.svg',
    width: 56,
  },
  {
    textColor: 'white',
    height: 66,
    url: 'https://marker.nanoka.fr/map_cluster-EFBB24-70.svg',
    width: 66,
  },
  {
    textColor: 'white',
    height: 78,
    url: 'https://marker.nanoka.fr/map_cluster-FF0000-80.svg',
    width: 78,
  },
  {
    textColor: 'white',
    height: 90,
    url: 'https://marker.nanoka.fr/map_cluster-6F3381-90.svg',
    width: 90,
  },
];
