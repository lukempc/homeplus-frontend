import { React } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import FormControl from '@mui/material/FormControl';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import TextField from '@mui/material/TextField';
import Checkbox from '@mui/material/Checkbox';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputAdornment from '@mui/material/InputAdornment';
import { updateField } from '../../../../../store/reducers/form/form.actions';
import { TextFieldGroup } from '../../../styles/TextFieldGroup.style';
import LocationFields from '../LocationFields';

const HandySelections = ({ values, handyErrors, updateFields }) => {
  const handleChange = (type) => (e) => {
    updateFields(type, e.target.value);
  };

  const handleCheckChange = () => {
    if (values.in_person) {
      updateFields('in_person', !values.in_person);
      updateFields('street', '');
      updateFields('state', '');
      updateFields('suburb', '');
      updateFields('postcode', '');
    } else {
      updateFields('in_person', !values.in_person);
    }
  };

  return (
    <div>
      <h4>Item Information</h4>
      <TextFieldGroup>
        <TextField
          required
          id="title"
          label="Item Name"
          variant="outlined"
          name="title"
          value={values.item_name}
          onChange={handleChange('item_name')}
          sx={{ width: 200 }}
          error={handyErrors && values.itemName.length < 1}
          helperText={handyErrors && values.itemName.length < 1 ? 'Must have name and value' : ' '}
        />
        <FormControl sx={{ m: 1, width: 200 }}>
          <InputLabel htmlFor="outlined-adornment-amount">Item Value</InputLabel>
          <OutlinedInput
            required
            id="itemValue"
            type="number"
            value={values.item_value}
            onChange={handleChange('item_value')}
            startAdornment={<InputAdornment position="start">$</InputAdornment>}
            label="Item Value"
            error={handyErrors && values.item_value <= 0}
          />
        </FormControl>
        <FormGroup>
          <FormControlLabel
            className="inPerson"
            control={<Checkbox onChange={handleCheckChange} checked={values.inPerson} />}
            label="In-person support"
          />
        </FormGroup>
      </TextFieldGroup>
      {values.in_person && <LocationFields />}
    </div>
  );
};

HandySelections.propTypes = {
  values: PropTypes.object.isRequired,
  updateFields: PropTypes.func,
  handyErrors: PropTypes.bool,
};

const mapDispatchToProps = (dispatch) => ({
  updateFields: (name, value) => dispatch(updateField(name, value)),
});

const mapStateToProps = ({ taskForm }) => ({
  values: taskForm,
});

export default connect(mapStateToProps, mapDispatchToProps)(HandySelections);
