import { useState } from 'react';
import AppApi from '../utils/axios';

const UseReview = () => {
  const [isReviewError, setError] = useState(false);
  const [isReviewLoading, setLoading] = useState(false);
  const [reviews, setReviews] = useState(null);

  const getReviewsByTasker = async (tasker_id) => {
    const data = await AppApi('get', `/tasker-reviews?id=${tasker_id}`);

    if (!data) {
      setError('Somthing went wrong');
      setLoading(false);
      return;
    }

    return data;
  };

  const requestTaskerReviews = async (tasker_id) => {
    setLoading(true);
    setError(false);

    const data = await getReviewsByTasker(tasker_id);

    setLoading(false);
    setReviews(data);
  };

  const createReview = async (values) => {
    const res = await AppApi('post', `/create-review`, values);
    return res;
  };

  return {
    isReviewError,
    isReviewLoading,
    reviews,
    requestTaskerReviews,
    createReview,
  };
};

export default UseReview;
