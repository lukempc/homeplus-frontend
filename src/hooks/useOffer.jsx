import { useState } from 'react';
import AppApi from '../utils/axios';

const UserOffer = () => {
  const [isOfferError, setError] = useState(false);
  const [isOfferLoading, setLoading] = useState(false);
  const [resaultOffer, setResaultOffer] = useState(null);
  const [offers, setOffers] = useState(null);

  const postOffer = async (values) => {
    const res = await AppApi('post', '/create-offer', values);
    return res;
  };

  const acceptOffer = (values) => {
    AppApi('put', `/accept-offer?id=${values.id}&reply_msg=${values.reply_msg}`);
  }

  const rejectOffer = (values) => {
    AppApi('put', `/reject-offer?id=${values.id}&reply_msg=${values.reply_msg}`);
  }

  const cancelOffer = (values) => {
    AppApi('put', `/cancel-offer?id=${values.id}&reply_msg=${values.reply_msg}`);
  }

  const getOffersByTask = async (task_id) => {
    const data = await AppApi('get', `/task-offers?id=${task_id}`);
    if (!data || !data.length) {
      setError('Somthing went wrong');
      setLoading(false);
      return;
    }

    return data;
  };

  const getOfferById = async (offer_id) => {
    const data = await AppApi('get', `/task-offer?id=${offer_id}`);

    if (!data) {
      setError('Somthing went wrong');
      setLoading(false);
      return;
    }

    return data;
  }

  const offersRequest = async (task_id) => {
    setLoading(true);
    setError(false);

    const data = await getOffersByTask(task_id);

    setLoading(false);
    setOffers(data);
  };

  const offerRequest = async (offer_id) => {
    setLoading(true);
    setError(false);

    const data = await getOfferById(offer_id);

    setLoading(false);
    setResaultOffer(data);
  }

  return {
    postOffer,
    acceptOffer,
    rejectOffer,
    cancelOffer,
    offersRequest,
    offerRequest,
    isOfferError,
    isOfferLoading,
    offers,
    resaultOffer
  };
};

export default UserOffer;
