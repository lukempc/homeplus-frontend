import styled from 'styled-components';

export const PostTask = styled.div`
  height: 93vh;
  background-color: white;
  overflow: hidden;
  position: relative;
  left: 50%;
  transform: translateX(-50%);
  @media (max-width: 768px) {
    overflow: scroll;
    overflow-x: hidden;
  }
`;
