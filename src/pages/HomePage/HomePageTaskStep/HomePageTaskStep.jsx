import React from 'react';
import { Box, Container, Wrapper } from './HomePageTaskStep.style';
import Checklist from '../../../assets/checklist.png';
import Budget from '../../../assets/budget.png';
import Worker from '../../../assets/choosing.png';
import TaskStep from './Components/TaskStep';

const HomePageTaskStep = ({ scrollPosition }) => (
  <Box>
    <Container>
      <h2>Post your first task in seconds</h2>
      <Wrapper scrollPosition={scrollPosition}>
        <TaskStep src={Checklist} number="1" children="Describe your task" />
        <TaskStep src={Budget} number="2" children="Set your budget" />
        <TaskStep src={Worker} number="3" children="Choose a best Tasker" />
      </Wrapper>
    </Container>
  </Box>
);

export default HomePageTaskStep;
