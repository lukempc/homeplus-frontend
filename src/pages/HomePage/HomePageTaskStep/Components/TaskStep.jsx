import React from 'react';
import { StepCard, StepStyle, Logo, StepNum, StepDescription } from './TaskStep.style';

const HomePageTaskStep = ({ src, number, children }) => {
  return (
    <StepCard>
      <StepStyle>
        <Logo src={src} />
        <StepNum>{number}</StepNum>
      </StepStyle>
      <StepDescription>{children}</StepDescription>
    </StepCard>
  );
};

export default HomePageTaskStep;
