import styled from 'styled-components';
import HomePageInfo from '../../../assets/homepage.jpg';

export const Box = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 500px;
  color: White;
  overflow: hidden;
  h1 {
    margin: 0px;
    font-size: 40px;
  }
  p {
    margin: 15px 0;
    font-size: 23px;
    line-height: 1.6;
  }
  background: linear-gradient(to right bottom, rgba(68, 68, 68, 0.63), rgba(68, 68, 68, 0.63)), url(${HomePageInfo});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  @media (max-width: 768px) {
    height: 600px;
    background-position: 60%;
    h1 {
      br {
        display: none;
      }
      font-size: 39px;
    }
    p {
      font-size: 23px;
    }
  }
  @keyframes typing {
    from {
      width: 0;
    }
  }
  @keyframes blink-caret {
    50% {
      border-color: transparent;
    }
  }
`;
export const Container = styled.div`
  display: flex;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;
export const PostTaskStyle = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  height: auto;
  padding: 0.3rem 1.6rem;
  color: White;
  font-size: 1rem;
  button {
    margin-top: 20px;
  }
`;
