import styled from 'styled-components';
export const Box = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 20px 0;
  padding: 40px 0;
`;
export const CardContainer = styled.div`
  display: flex;
  flex-direction: row;
  max-width: 1000px;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  margin: 20px 0 50px;
  @media (max-width: 768px) {
    width: 100%;
  }
`;
