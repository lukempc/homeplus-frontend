import styled from 'styled-components';
export const Box = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  margin: 80px 0;
  h2 {
    margin-bottom: 40px;
    padding: 10px;
  }
`;

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  margin: 20px 0 50px;
  gap: 40px;
  @media (max-width: 1320px) {
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  @media (max-width: 768px) {
    > div {
      max-width: 350px;
    }
  }
`;
