import TaskFormActionTypes from './form.types';

export const updateField = (name, value) => {
  return {
    type: TaskFormActionTypes.UPDATE_FIELD,
    payload: { name, value },
  };
};

export const backToInit = () => ({
  type: TaskFormActionTypes.BACK_TO_INIT,
});

export const toggleField = () => ({
  type: TaskFormActionTypes.TOGGLE_FIELD,
});
